Cookie Clicker Multiplayer
--------------

This is a twist on the Cookie clicker game: http://orteil.dashnet.org/cookieclicker/

Disclaimer from the original code:
```
All this code is copyright Orteil, 2013-2017.
	-with some help, advice and fixes by Nicholas Laux, Debugbro and Opti
	-also includes a bunch of snippets found on stackoverflow.com
Hello, and welcome to the joyous mess that is main.js. Code contained herein is not guaranteed to be good, consistent, or sane. Most of this is years old at this point and harkens back to simpler, cruder times. Have a nice trip.
```

##Install
Install node.js: http://nodejs.org/

Then ```npm install```

Run ```node main.js``` and visit http://localhost:3000

##The Game
Based on the original cookie clicker, this version adds a multiplayer addition where you can buy and send attacks to other players. Attacks reload with time. The goal is to reach a certain number of cookies.

##TODO
- lots of bug
- move front end actions to backend
- protect against page refresh
- protect from injections

