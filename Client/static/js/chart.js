var points = 'none';
var interpolation = 'linear';

// create a dataset with items
var dataset = new vis.DataSet({
	type: {start: 'ISODate', end: 'ISODate' }
});
var legends = [
//	"startDate",
//	"lastDate",
	{code:"cookies",options:{},title:"Cookies"},
	{code:"cookiesEarned",options:{},title:"Cookies Earned"},
	{code:"cookieClicks",options:{yAxisOrientation: 'right'},title:"Cookies Clicks"},
	{code:"goldenClicks",options:{yAxisOrientation: 'right'},title:"Golden Clicks"},
	{code:"handmadeCookies",options:{},title:"Handmade Cookies"},
	{code:"missedGoldenClicks",options:{yAxisOrientation: 'right'},title:"Missed Golden Clicks"},
	{code:"BuildingsOwned",options:{yAxisOrientation: 'right'},title:"Building"},
	{code:"UpgradesOwned",options:{yAxisOrientation: 'right'},title:"Upgrades"},
	{code:"AchievementsOwned",options:{yAxisOrientation: 'right'},title:"Achievement"},
]
var groups = new vis.DataSet();
legends.forEach(function(l){groups.add({id:l['code'],content:l['title'],options:l['options']});});

function Get(url)
{
	var Httpreq = new XMLHttpRequest();
	Httpreq.open("GET",url,false);
	Httpreq.send(null);
	return Httpreq.responseText;
}
function getJSON(url)
{
	return JSON.parse(Get(url));
}

function formatData(data)
{
	var newData = [];
	var start,end;
	for (var i = 0; i < data.length; i++)
	{
		var dd = data[i];
		if(i===0)
			start=dd["startDate"];
		else if(i===data.length-1)
			end=dd["lastDate"];
		legends.forEach(function(l){
			newData.push({group:l['code'], x: dd['lastDate'], y:dd[l['code']] });
		});

	}
	console.log('Start, End',start,end);
	console.log('To print',newData);
	graph2d.setWindow(start,end);
	loadDataIntoVis(newData);
}
function createData()
{
	var items = [
	  ];
	  legends.forEach(function(l){
	  	items.push({x: '2016-02-16', y: 0, group: l['code']});
	  	items.push({x: '2016-02-21', y: 5, group: l['code']});
	  });

	  loadDataIntoVis(items)
}

function loadDataIntoVis(data) {
	dataset.clear();
	dataset.add(data);
}

var $lba= document.querySelectorAll('.drawPlayer');

for(var i=0; i<$lba.length; i++)
{
    $lba[i].addEventListener("click", function(e){formatData(getJSON('/player/'+e.srcElement.value));}, false);
}

function toggleRedraw()
{
	var switches = document.querySelectorAll('.chart-switches');
	var groupsVisibility={};
	for(var i=0; i<switches.length; i++)
	{
		var sw = switches[i];
		groupsVisibility[sw['value']]= sw.checked;
	}

	graph2d.setOptions({
		groups:{
			visibility:groupsVisibility
		}
	})
}

var container = document.getElementById('visualization');
var containertoggle = document.getElementById('charttoggle');
legends.forEach(function(l){
	var node = document.createElement('tr');
	node.innerHTML = '<td>'+l['title']+'</td><td><input type="checkbox" value="'+l['code']+'" class="chart-switches" checked></td>';
	containertoggle.appendChild(node)
});
var switches = document.querySelectorAll('.chart-switches');
for(var i=0; i<switches.length; i++)
{
	switches[i].addEventListener('click',toggleRedraw);
}
var options = {
	sampling: true,
	drawPoints: {enabled:false, size:3},
	interpolation: false,
	legend: true,
	width:  '100%',
    height: '700px',
	start: '2016-02-16',
      end: '2016-02-21'
};

var graph2d = new vis.Graph2d(container, dataset,groups, options);

createData();