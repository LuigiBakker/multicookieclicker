var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var Game = require('./cookie_game')

app.use(express.static('Client/static'));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/Client/index.html');
});

app.set('view engine', 'jade');

app.get('/players', function (req, res) {
  res.render('players', { title: 'Players', players: {"127.0.0.1":'A',"192.168.0.3":'B'}});
});

app.get('/player/:IP', function (req, res)
{
	var IP = req.params.IP;
	console.log("Player:",IP)
	var history = Game.GetPlayerHistory(IP);
	res.json(history);
});

var connectedIPs = [];
var bannedIPs=[];
var lastMessages=[];
var challengesData=[];

io.on('connection', function(socket)
{
	function sendMessage(dmsg,all)
	{
		if(all)
			io.sockets.emit('chat message', dmsg);
		else
			socket.broadcast.emit('chat message', dmsg);
		lastMessages.push(dmsg);
		if(lastMessages.length>10)
			lastMessages.splice(0,1);
	}

	socket.name = null;
	console.log("New connection from ", socket.request.connection.remoteAddress);
	socket.on('init',function(user)
	{
		if(typeof user === "string" && user != '')
		{
			socket.name = user;
			socket.IP=socket.request.connection.remoteAddress;
			if(bannedIPs.indexOf(socket.IP) != -1)
			{
				socket.emit('banned');
				return;//banned player
			}

			if(connectedIPs.indexOf(socket.IP) === -1 && Game.is_running)
			{
				var playerData = Game.InitPlayer(user,socket.request.connection.remoteAddress, socket.id);
				console.log("Sending", playerData)
				socket.emit('init_ack', playerData);
				setTimeout(function(){
					for(var i=0;i<lastMessages.length;++i)
					{
						socket.emit('chat message', lastMessages[i]);
					}
				},500)
				connectedIPs.push(socket.IP);
			}
			console.log("Connected players:", connectedIPs);
		}
	});
	socket.on('disconnect', function() {

		var i = connectedIPs.indexOf(socket.IP);
		console.log('Player Disconnect:', socket.IP, i);
		if(i != -1)
		{
			connectedIPs = connectedIPs.filter(function(IP){return IP!=socket.IP;});
			socket.broadcast.emit('player disc', socket.IP);
		}
		console.log("Connected players:", connectedIPs);
   });


	socket.on('save',function(data){
		if(bannedIPs.indexOf(socket.IP) != -1)
			return;//banned player
		var updateValue = Game.UpdatePlayer(socket.name,socket.IP, data);
		//Y Ok fine
		//S Stop
		//B Banned
		//W Win!
		switch(updateValue)
		{
			case 'Y':
				var uniqueconnectedIPs = connectedIPs.filter(function(item, pos) {
					return connectedIPs.indexOf(item) == pos && item != socket.IP;
				})
				playerList=Game.GetPlayerCookieCount(uniqueconnectedIPs);
				console.log("Sending player list to :",socket.IP, playerList);
				socket.emit('players list', playerList);
				break;
			case 'B':
				console.log("Banning player:", socket.IP);
				socket.emit('banned')
				var msg='User '+socket.name+' got banned for trying to cheat. His/her IP is '+socket.IP+', let him know the pain'
				var dmsg = {'user':'System','msg':msg,'system':true};
				sendMessage(dmsg,false)
				bannedIPs.push(socket.IP);
				var i;
				while((i = connectedIPs.indexOf(socket.IP)) != -1)
					connectedIPs.splice(i,1);
				break;
			case 'S':
				sendMessage({'user':'System','system':true,msg:"Time to pause the game"}, true);
				io.sockets.emit('pause');console.log("Restart the Game")
				var toStart = new Date();
				toStart.setMilliseconds(0);
				toStart.setSeconds(0);
				toStart.setMinutes(0);
				if(toStart.getHours()>17)
					toStart.setDate(toStart.getDate()+1);
				toStart.setHours(9);
				var waitTime = toStart.getTime() - Date.now();
				setTimeout(function()
					{
					Game.is_running=true;
					sendMessage({'user':'System','system':true,msg:"Time to restart the game"}, true);
					io.sockets.emit('start');
					}, waitTime);
				break;
			case 'W':
				console.log("We got a winner!");
				sendMessage({'user':'System','system':true,msg:"Congratulation to "+socket.name+" for winning the game!"}, true);
				socket.broadcast.emit('lose',socket.name);
				socket.emit('win');
				break;
			default:
				//Nothing
		}
	});

	socket.on('chat message', function(msg)
	{
		console.log("Chat message:", msg)
		if(socket.name != null)
		{
			var dmsg = {'user':socket.name,'msg':msg};
			sendMessage(dmsg,false)
		}
	});

	socket.on('challengeClick',function(challengeIDs)
	{
		challengeIDs.forEach(function(challengeID)
		{
			challengesData[challengeID][socket.IP]++;
			console.log('Received click from '+socket.IP+' for challenge['+ challengeID+']',challengesData[challengeID]);
		});
	});

	socket.on('attack', function(msg)
	{
		console.log("Attack received", msg);
		//{attackid:this.id,attackname:this.name,target:IP,tier:this.tier}
		var attacker = Game.GetPlayer(socket.IP);
		var victim = Game.GetPlayer(msg["target"]);
		sendMessage({'user':'System','system':true,msg:attacker["name"]+" is attacking "+victim["name"]+" with "+msg["attackname"]}, true);
		io.to(victim["ID"]).emit('attack_rcv', msg);
		if(msg["attackname"]==="Steal")
			socket.emit('voleur', Math.floor(victim["datal"].cookies*0.05*msg["tier"]));
		else if(msg["attackname"]==="Challenge")
		{
			var toStart = new Date();
			toStart.setMilliseconds(0);
			toStart.setSeconds(0);
			toStart.setMinutes(toStart.getMinutes()+1);
			var toStartString = toStart.getHours()+':'+(toStart.getMinutes()<10?'0':'')+toStart.getMinutes()+':00';
			socket.emit('notif',{title:'Challenge', text:'You have challenged '+victim["name"]+'. Challenge will start at '+toStartString+'. Be ready!'});
			io.to(victim["ID"]).emit('notif',{title:'Challenge', text:'You have been challenged by '+attacker["name"]+'. Challenge will start at '+toStartString+'. Be ready!'});
			var waitTime = toStart.getTime() - Date.now();
			setTimeout(function(){
				socket.emit('alert','Your challenge is about to start in 5secs');
				io.to(victim["ID"]).emit('alert','Your challenge is about to start in 5secs');
				setTimeout(function()
				{
					//challenge started
					console.log('Challenge started between '+attacker["IP"]+' and '+victim["IP"])
					var challengeData = {};
					challengeData[attacker["IP"]] = 0;
					challengeData[victim["IP"]] = 0;
					var nthis_challenge = challengesData.push(challengeData) - 1;
					var this_challenge = challengesData[nthis_challenge];
					socket.emit('challenge_started',nthis_challenge);
					io.to(victim["ID"]).emit('challenge_started',nthis_challenge);
					setTimeout(function()
					{
						//challenge finished
						console.log('Challenge finished between '+attacker["IP"]+' and '+victim["IP"], this_challenge);
						var results_string = attacker["name"]+': '+this_challenge[attacker["IP"]]+'<br>'+victim["name"]+': '+this_challenge[victim["IP"]];
						if (this_challenge[attacker["IP"]] === this_challenge[victim["IP"]])
						{
							//no winner
							socket.emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:false});
							io.to(victim["ID"]).emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:false});

						}
						else if (this_challenge[attacker["IP"]] > this_challenge[victim["IP"]])
						{
							//attacker win
							socket.emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:false});
							io.to(victim["ID"]).emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:true});
						}
						else
						{
							//victim win
							socket.emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:true});
							io.to(victim["ID"]).emit('challenge_finished',{results:results_string,challengeID:nthis_challenge,tier:msg["tier"],lose:false});
						}
					},15000);
				},5000);
			}, waitTime-5000);
		}
    })
});

http.listen(3000, "0.0.0.0", function()
{
	console.log('listening on *:3000');
});