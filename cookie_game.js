var fs = require('fs');


/*=====================================================================================
MISC HELPER FUNCTIONS
=======================================================================================*/
function l(what) {return document.getElementById(what);}
function choose(arr) {return arr[Math.floor(Math.random()*arr.length)];}

function escapeRegExp(str){return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");}
function replaceAll(find,replace,str){return str.replace(new RegExp(escapeRegExp(find),'g'),replace);}


if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {return i;}
        }
        return -1;
    };
}

function randomFloor(x) {if ((x%1)<Math.random()) return Math.floor(x); else return Math.ceil(x);}

function shuffle(array)
{
	var counter = array.length, temp, index;
	// While there are elements in the array
	while (counter--)
	{
		// Pick a random index
		index = (Math.random() * counter) | 0;

		// And swap the last element with it
		temp = array[counter];
		array[counter] = array[index];
		array[index] = temp;
	}
	return array;
}

var sinArray=[];
for (var i=0;i<360;i++)
{
	//let's make a lookup table
	sinArray[i]=Math.sin(i/360*Math.PI*2);
}
function quickSin(x)
{
	//oh man this isn't all that fast actually
	//why do I do this. why
	var sign=x<0?-1:1;
	return sinArray[Math.round(
		(Math.abs(x)*360/Math.PI/2)%360
	)]*sign;
}


//Beautify and number-formatting adapted from the Frozen Cookies add-on (http://cookieclicker.wikia.com/wiki/Frozen_Cookies_%28JavaScript_Add-on%29)
function formatEveryThirdPower(notations)
{
	return function (value)
	{
		var base = 0,
		notationValue = '';
		if (value >= 1000000 && isFinite(value))
		{
			value /= 1000;
			while(Math.round(value) >= 1000)
			{
				value /= 1000;
				base++;
			}
			if (base>=notations.length) {return 'Infinity';} else {notationValue = notations[base];}
		}
		return ( Math.round(value * 1000) / 1000 ) + notationValue;
	};
}

function rawFormatter(value) {return Math.round(value * 1000) / 1000;}

var numberFormatters =
[
	rawFormatter,
	formatEveryThirdPower([
		'',
		' million',
		' billion',
		' trillion',
		' quadrillion',
		' quintillion',
		' sextillion',
		' septillion',
		' octillion',
		' nonillion',
		' decillion',
		' undecillion',
		' duodecillion',
		' tredecillion',
		' quattuordecillion',
		' quindecillion'
	]),
	formatEveryThirdPower([
		'',
		' M',
		' B',
		' T',
		' Qa',
		' Qi',
		' Sx',
		' Sp',
		' Oc',
		' No',
		' Dc',
		' UnD',
		' DoD',
		' TrD',
		' QaD',
		' QiD'
	])
];

function Beautify(value,floats)
{
	var negative=(value<0);
	var decimal='';
	if (value<1000000 && floats>0 && Math.floor(value.toFixed(floats))!=value.toFixed(floats)) decimal='.'+(value.toFixed(floats).toString()).split('.')[1];
	value=Math.floor(Math.abs(value));
	var formatter=numberFormatters[Game.prefs.format?0:1];
	var output=formatter(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g,',');
	return negative?'-'+output:output+decimal;
}

function utf8_to_b64( str ) {
	try{
		return Base64.encode(unescape(encodeURIComponent( str )));
	}
	catch(err)
	{return '';}
}

function b64_to_utf8( str ) {
	try{
//		return decodeURIComponent(escape(Base64.decode( str )));
		return decodeURIComponent(escape((new Buffer( str,'base64')).toString('utf8')));
	}
	catch(err)
	{
		return '';
	}
}


function CompressBin(arr)//compress a sequence like [0,1,1,0,1,0]... into a number like 54.
{
	var str='';
	var arr2=arr.slice(0);
	arr2.unshift(1);
	arr2.push(1);
	arr2.reverse();
	for (var i in arr2)
	{
		str+=arr2[i];
	}
	str=parseInt(str,2);
	return str;
}

function UncompressBin(num)//uncompress a number like 54 to a sequence like [0,1,1,0,1,0].
{
	var arr=num.toString(2);
	arr=arr.split('');
	arr.reverse();
	arr.shift();
	arr.pop();
	return arr;
}

function CompressLargeBin(arr)//we have to compress in smaller chunks to avoid getting into scientific notation
{
	var arr2=arr.slice(0);
	var thisBit=[];
	var bits=[];
	for (var i in arr2)
	{
		thisBit.push(arr2[i]);
		if (thisBit.length>=50)
		{
			bits.push(CompressBin(thisBit));
			thisBit=[];
		}
	}
	if (thisBit.length>0) bits.push(CompressBin(thisBit));
	arr2=bits.join(';');
	return arr2;
}

function UncompressLargeBin(arr)
{
	var arr2=arr.split(';');
	var bits=[];
	for (var i in arr2)
	{
		bits.push(UncompressBin(parseInt(arr2[i])));
	}
	arr2=[];
	for (var i in bits)
	{
		for (var ii in bits[i]) arr2.push(bits[i][ii]);
	}
	return arr2;
}


function pack(bytes) {
    var chars = [];
	var len=bytes.length;
    for(var i = 0, n = len; i < n;) {
        chars.push(((bytes[i++] & 0xff) << 8) | (bytes[i++] & 0xff));
    }
    return String.fromCharCode.apply(null, chars);
}

function unpack(str) {
    var bytes = [];
	var len=str.length;
    for(var i = 0, n = len; i < n; i++) {
        var char = str.charCodeAt(i);
        bytes.push(char >>> 8, char & 0xFF);
    }
    return bytes;
}

//modified from http://www.smashingmagazine.com/2011/10/19/optimizing-long-lists-of-yesno-values-with-javascript/
function pack2(/* string */ values) {
    var chunks = values.match(/.{1,14}/g), packed = '';
    for (var i=0; i < chunks.length; i++) {
        packed += String.fromCharCode(parseInt('1'+chunks[i], 2));
    }
    return packed;
}

function unpack2(/* string */ packed) {
    var values = '';
    for (var i=0; i < packed.length; i++) {
        values += packed.charCodeAt(i).toString(2).substring(1);
    }
    return values;
}



//seeded random function, courtesy of http://davidbau.com/archives/2010/01/30/random_seeds_coded_hints_and_quintillions.html
(function(a,b,c,d,e,f){function k(a){var b,c=a.length,e=this,f=0,g=e.i=e.j=0,h=e.S=[];for(c||(a=[c++]);d>f;)h[f]=f++;for(f=0;d>f;f++)h[f]=h[g=j&g+a[f%c]+(b=h[f])],h[g]=b;(e.g=function(a){for(var b,c=0,f=e.i,g=e.j,h=e.S;a--;)b=h[f=j&f+1],c=c*d+h[j&(h[f]=h[g=j&g+b])+(h[g]=b)];return e.i=f,e.j=g,c})(d)}function l(a,b){var e,c=[],d=(typeof a)[0];if(b&&"o"==d)for(e in a)try{c.push(l(a[e],b-1))}catch(f){}return c.length?c:"s"==d?a:a+"\0"}function m(a,b){for(var d,c=a+"",e=0;c.length>e;)b[j&e]=j&(d^=19*b[j&e])+c.charCodeAt(e++);return o(b)}function n(c){try{return a.crypto.getRandomValues(c=new Uint8Array(d)),o(c)}catch(e){return[+new Date,a,a.navigator.plugins,a.screen,o(b)]}}function o(a){return String.fromCharCode.apply(0,a)}var g=c.pow(d,e),h=c.pow(2,f),i=2*h,j=d-1;c.seedrandom=function(a,f){var j=[],p=m(l(f?[a,o(b)]:0 in arguments?a:n(),3),j),q=new k(j);return m(o(q.S),b),c.random=function(){for(var a=q.g(e),b=g,c=0;h>a;)a=(a+c)*d,b*=d,c=q.g(1);for(;a>=i;)a/=2,b/=2,c>>>=1;return(a+c)/b},p},m(c.random(),b)})(this,[],Math,256,6,52);

function bind(scope,fn)
{
	//use : bind(this,function(){this.x++;}) - returns a function where "this" refers to the scoped this
	return function() {fn.apply(scope,arguments);};
}


/*=====================================================================================
GAME INITIALIZATION
=======================================================================================*/
var Game={};
Game.is_running=true;
Game.history_players={};

Game.Init=function()
{
	Game.players={}
	Game.haswinner=false;
	fs.readFile('players.json', 'utf8', function (err, data)
	{
		if (err)
			console.log('No previous history'); // we'll not consider error handling for now
		else
		{
			Game.history_players = JSON.parse(data);
			for(var IP in Game.history_players)
			{
				var player_data = Game.history_players[IP][Game.history_players[IP].length -1];
				Game.players[IP]={IP:IP,data:player_data};
				Game.players[IP]['datal']=Game.LoadData(player_data);
			}
		}
	});
}

Game.GetPlayerHistory=function(IP)
{
	if(IP === undefined)
		return [];
	var history = Game.history_players[IP] || [];
	if(history === [])
		return [];
	console.log("History found for ", IP)
	history =  history.map(function(h){
		var lh = Game.LoadData(h);
		var legends = ["startDate","lastDate","cookies","cookiesEarned","Cookies Earned","cookieClicks","goldenClicks","handmadeCookies","missedGoldenClicks","BuildingsOwned","UpgradesOwned","AchievementsOwned"]
		for(var key in lh)
		{
			if(legends.indexOf(key) === -1)
				delete lh[key]
		}
		return lh;
	});
	return history;
}

Game.LoadData=function(data)
{
	var i =0;
	var str='';
	var outData={};
	if (data)
		str=unescape(data);

	if (str!='')
	{
		var version=0;
		str=str.split('!END!')[0];
		str=b64_to_utf8(str);
		if (str!='')
		{
			var spl='';
			str=str.split('|');
			version=parseFloat(str[0]);
//			console.log("loading:",str)
			if (isNaN(version) || str.length<5)
			{
				return outData;
			}
			if (version>=1)
			{
				spl=str[2].split(';');//save stats
				outData.startDate=parseInt(spl[0]);
				outData.fullDate=parseInt(spl[1]);
				outData.lastDate=parseInt(spl[2]);
				outData.bakeryName=spl[3]?spl[3]:'';
				//prefs
				outData.prefs={};
				if (version<1.0503) spl=str[3].split('');
				else spl=unpack2(str[3]).split('');
				outData.prefs.particles=parseInt(spl[0]);
				outData.prefs.numbers=parseInt(spl[1]);
				outData.prefs.autosave=parseInt(spl[2]);
				outData.prefs.autoupdate=spl[3]?parseInt(spl[3]):1;
				outData.prefs.milk=spl[4]?parseInt(spl[4]):1;
				outData.prefs.fancy=parseInt(spl[5]);
				outData.prefs.warn=spl[6]?parseInt(spl[6]):0;
				outData.prefs.cursors=spl[7]?parseInt(spl[7]):0;
				outData.prefs.focus=spl[8]?parseInt(spl[8]):0;
				outData.prefs.format=spl[9]?parseInt(spl[9]):0;
				outData.prefs.notifs=spl[10]?parseInt(spl[10]):0;
				outData.prefs.wobbly=spl[11]?parseInt(spl[11]):0;
				outData.prefs.monospace=spl[12]?parseInt(spl[12]):0;
				spl=str[4].split(';');//cookies and lots of other stuff
				outData.cookies=parseFloat(spl[0]);
				outData.cookiesEarned=parseFloat(spl[1]);
				outData.cookieClicks=spl[2]?parseInt(spl[2]):0;
				outData.goldenClicks=spl[3]?parseInt(spl[3]):0;
				outData.handmadeCookies=spl[4]?parseFloat(spl[4]):0;
				outData.missedGoldenClicks=spl[5]?parseInt(spl[5]):0;
				outData.backgroundType=spl[6]?parseInt(spl[6]):0;
				outData.milkType=spl[7]?parseInt(spl[7]):0;
				outData.cookiesReset=spl[8]?parseFloat(spl[8]):0;
				outData.elderWrath=spl[9]?parseInt(spl[9]):0;
				outData.pledges=spl[10]?parseInt(spl[10]):0;
				outData.pledgeT=spl[11]?parseInt(spl[11]):0;
				outData.nextResearch=spl[12]?parseInt(spl[12]):0;
				outData.researchT=spl[13]?parseInt(spl[13]):0;
				outData.resets=spl[14]?parseInt(spl[14]):0;
				outData.goldenClicksLocal=spl[15]?parseInt(spl[15]):0;
				outData.cookiesSucked=spl[16]?parseFloat(spl[16]):0;
				outData.wrinklersPopped=spl[17]?parseInt(spl[17]):0;
				outData.santaLevel=spl[18]?parseInt(spl[18]):0;
				outData.reindeerClicked=spl[19]?parseInt(spl[19]):0;
				outData.seasonT=spl[20]?parseInt(spl[20]):0;
				outData.seasonUses=spl[21]?parseInt(spl[21]):0;
				outData.season=spl[22]?spl[22]:'';
				var wrinklers={amount:spl[23]?spl[23]:0,number:spl[24]?spl[24]:0};
				outData.prestige=spl[25]?parseFloat(spl[25]):0;
				outData.heavenlyChips=spl[26]?parseFloat(spl[26]):0;
				outData.heavenlyChipsSpent=spl[27]?parseFloat(spl[27]):0;
				outData.heavenlyCookies=spl[28]?parseFloat(spl[28]):0;
				outData.ascensionMode=spl[29]?parseInt(spl[29]):0;
				outData.permanentUpgrades=[];
				outData.permanentUpgrades[0]=spl[30]?parseInt(spl[30]):-1;
				outData.permanentUpgrades[1]=spl[31]?parseInt(spl[31]):-1;
				outData.permanentUpgrades[2]=spl[32]?parseInt(spl[32]):-1;
				outData.permanentUpgrades[3]=spl[33]?parseInt(spl[33]):-1;
				outData.permanentUpgrades[4]=spl[34]?parseInt(spl[34]):-1;
				outData.dragonLevel=spl[35]?parseInt(spl[35]):0;
				outData.dragonAura=spl[36]?parseInt(spl[36]):0;
				outData.dragonAura2=spl[37]?parseInt(spl[37]):0;
				outData.chimeType=spl[38]?parseInt(spl[38]):0;
				outData.volume=spl[39]?parseInt(spl[39]):50;
				spl=str[5].split(';');//buildings
				outData.BuildingsOwned=0;
				for (var i=0;i<spl.length;++i)
				{
					if(spl[i])
					{
						var mestr=spl[i].toString().split(',');
						outData.BuildingsOwned+=parseInt(mestr[0]);
					}
				}
				if (str[6])
					spl=str[6];
				else
					spl=[];//upgrades
				spl=unpack2(spl).split('');
				outData.UpgradesOwned=0;
				for (var i=0;i<spl.length;++i)
				{
					if(spl[i])
					{
						var mestr=spl[i].toString().split(',');
						outData.UpgradesOwned+=parseInt(mestr[0]);
					}
				}
				if (str[7])
					spl=str[7];
				else
					spl=[];//achievements
				spl=unpack2(spl).split('');
				outData.AchievementsOwned=0;
				for (var i=0;i<spl.length;++i)
				{
					if (spl[i])
					{
						var mestr=[spl[i]];
						var won=parseInt(mestr[0]);
						if(won)
							outData.AchievementsOwned++;
					}
				}

				if (outData.backgroundType==-1) outData.backgroundType=0;
				if (outData.milkType==-1) outData.milkType=0;

				//advance timers
				var framesElapsed=Math.ceil(((new Date().getTime()-outData.lastDate)/1000)*outData.fps);
				if (outData.pledgeT>0) outData.pledgeT=Math.max(outData.pledgeT-framesElapsed,1);
				if (outData.seasonT>0) outData.seasonT=Math.max(outData.seasonT-framesElapsed,1);
				if (outData.researchT>0) outData.researchT=Math.max(outData.researchT-framesElapsed,1);

				//recompute prestige
				outData.HCfactor=3;
				outData.prestige=Math.floor(Math.pow(outData.cookiesReset/1000000000000,1/outData.HCfactor));
				if ((outData.heavenlyChips+outData.heavenlyChipsSpent)<outData.prestige)
				{
					outData.heavenlyChips=outData.prestige;
					outData.heavenlyChipsSpent=0;
				}//chips owned and spent don't add up to total prestige? set chips owned to prestige
			}

			outData.TickerAge=0;
			outData.elderWrathD=0;
			outData.frenzy=0;
			outData.frenzyPower=1;
			outData.frenzyMax=0;
			outData.clickFrenzy=0;
			outData.clickFrenzyPower=1;
			outData.clickFrenzyMax=0;
			outData.recalculateGains=1;
			outData.storeToRefresh=1;
			outData.upgradesToRebuild=1;
		}
	}
	return outData;
}

Game.IsCheating=function(old_data, new_data)
{
	if (old_data["cookies"] === undefined || new_data["cookies"] === undefined)
		return false;
	console.log("is Cheating?", old_data.cookies, new_data.cookiesEarned)
	if(new_data.cookies > (old_data.cookiesEarned+1)*10000)
		return true;
	return false;
}

Game.UpdatePlayer=function(name, IP, data)
{
	var update_status = '';
	if(data && Game.players[IP] != undefined && Game.is_running)
	{
		player = Game.players[IP];
		if(!player['cheat'])
		{
			if(Game.history_players[IP] === undefined)
				Game.history_players[IP] = [];
			Game.history_players[IP].push(data);
			fs.writeFile('players.json', JSON.stringify(Game.history_players, null, 4), function(err) {
				if(err) {
				  console.log(err);
				} else {
				  console.log("History saved");
				}
			});

			new_data = Game.LoadData(data)
			old_data = player['datal']
			if (Game.IsCheating(old_data, new_data))
			{
				console.log("Player trying to cheat the game:", name, "\tOld cookies:", old_data.cookies, "Vs",new_data.cookies)
				player['cheat']=true;
				update_status='B';
			}
			else
			{
				console.log("Player saving:", name, "on IP", IP)
				player['data']= data;
				player['datal']= new_data;
				update_status = 'Y';
				if(new_data["cookies"] > 1000000000000000 && !Game.haswinner)
				{
					Game.haswinner=true;
					update_status='W';
				}
				var hh = (new Date()).getHours();
//				if(hh<9 || 17<hh)
				if(false)//DEBUG
				{
					Game.is_running=false;
					update_status='S';
				}
			}
		}
	}

	return update_status;
}

Game.GetPlayerCookieCount=function(IPsconnected)
{
	var plist=[];
	for(var i=0;i<IPsconnected.length;++i)
	{
		var player=Game.players[IPsconnected[i]];
		var cookieplayer=0;
		if(player['datal']['cookies']!=undefined)
			cookieplayer=Math.floor(player['datal']['cookies']);
		plist.push({'IP':player.IP,'cookies':cookieplayer,'name':player["name"]});
	}
	return plist;
}

Game.GetPlayer=function(IP)
{
	return Game.players[IP];
}

Game.InitPlayer=function(name, IP, socketid)
{
	var playerData='';
	if(Game.players[IP] === undefined)
	{
		console.log("Welcome: ", name, IP)
		Game.players[IP]={}
		Game.players[IP]['IP']=IP;
		Game.players[IP]['ID']=socketid;
		Game.players[IP]['name']=name;
		Game.players[IP]['data']='';
		Game.players[IP]['datal']={};
		Game.players[IP]['cheat']=false;
	}
	else
	{
		Game.players[IP]['ID']=socketid;
		Game.players[IP]['name']=name;
		console.log("Welcome Back: ", name, IP, "loading")
		playerData=Game.players[IP]['data'];
	}
	return playerData;
}

Game.Init()

module.exports = Game